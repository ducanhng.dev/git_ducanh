package dto

import (
	"time"

	_ "github.com/go-playground/validator/v10"
)

type ProjectRequest struct {
	Name             string `json:"name" binding:"required"`
	Category         string `json:"category" binding:"required,Enum=client_non-billable_system"`
	ProjectSpend     int    `json:"project_spend" binding:"required,numeric"`
	ProjectVariance  int    `json:"project_variance" binding:"required,numeric"`
	ProjectStartedAt string `json:"project_started_at" binding:"required,Date,StartDate"`
	ProjectEndedAt   string `json:"project_ended_at" binding:"required,Date"`
}

type ProjectResponse struct {
	ID                uint      `json:"id,omitempty"`
	Name              string    `json:"name"`
	Category          string    `json:"category"`
	ProjectSpend      int       `json:"project_spend"`
	ProjectVariance   int       `json:"project_variance"`
	RevenueRecognised int       `json:"revenue_recognised"`
	ProjectStartedAt  time.Time `json:"project_started_at"`
	ProjectEndedAt    time.Time `json:"project_ended_at"`
}
type ProjectDetailResponse struct {
	ID                uint      `json:"id,omitempty"`
	Name              string    `json:"name"`
	Category          string    `json:"category"`
	ProjectSpend      int       `json:"project_spend"`
	ProjectVariance   int       `json:"project_variance"`
	RevenueRecognised int       `json:"revenue_recognised"`
	ProjectStartedAt  time.Time `json:"project_started_at"`
	ProjectEndedAt    time.Time `json:"project_ended_at"`
	CreatedAt         time.Time `json:"create_at"`
	UpdatedAt         time.Time `json:"updated_at"`
	DeletedAt         time.Time `json:"deteled_at"`
}
