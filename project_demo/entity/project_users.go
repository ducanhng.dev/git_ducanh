package entity

import (
	"time"

	"gorm.io/gorm"
)

type ProjectUsers struct {
	ID        uint           `gorm:"column:id;type:uint;primaryKey;autoIncrement"`
	UserID    uint           `gorm:"column:user_id;type:uint"`
	ProjectID uint           `gorm:"column:project_id;type:uint"`
	CreatedAt time.Time      `gorm:"column:created_at;type:datetime;autoCreateTime:milli"`
	UpdatedAt time.Time      `gorm:"column:updated_at;type:datetime;autoUpdateTime:milli"`
	DeletedAt gorm.DeletedAt `gorm:"column:deleted_at;type:datetime"`
	Users     []User         `gorm:"foreignKey:UserID;references:ID"`
	Projects  []Project      `gorm:"foreignKey:ProjectID;references:ID"`
}
