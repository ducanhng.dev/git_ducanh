package entity

import (
	"time"
)

type User struct {
	ID        uint      `gorm:"column:id;type:uint;primarykey;autoIncrement"`
	FirstName string    `gorm:"column:first_name;type:varchar(255)"`
	LastName  string    `gorm:"column:last_name;type:varchar(255)"`
	Email     string    `gorm:"column:email;type:varchar(255)"`
	Password  string    `gorm:"column:password;type:varchar(255)"`
	CreatedAt time.Time `gorm:"column:created_at;type:datetime;autoCreateTime:milli"`
	UpdatedAt time.Time `gorm:"column:updated_at;type:datetime;autoUpdateTime:milli"`
	Projects  []Project `gorm:"many2many:project_users"`
}
