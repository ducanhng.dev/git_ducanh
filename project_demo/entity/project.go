package entity

import (
	"time"

	"gorm.io/gorm"
)

type Project struct {
	ID                uint           `gorm:"column:id;type:uint;primarykey;autoIncrement"`
	Name              string         `gorm:"column:name;type:varchar(255);not null"`
	Category          string         `gorm:"column:category;type:varchar(255);not null;default='client'"`
	ProjectSpend      int            `gorm:"column:project_spend;type:int;not null;default=0;"`
	ProjectVariance   int            `gorm:"column:project_variance;type:int;not null;default=0;"`
	RevenueRecognised int            `gorm:"column:revenue_recognised;type:int;not null;default=0;"`
	ProjectStartedAt  time.Time      `gorm:"column:project_started_at;type:datetime;not null"`
	ProjectEndedAt    *time.Time     `gorm:"column:project_ended_at;type:datetime"`
	CreatedAt         time.Time      `gorm:"column:created_at;type:datetime;autoCreateTime:milli"`
	UpdatedAt         time.Time      `gorm:"column:updated_at;type:datetime;autoUpdateTime:milli"`
	DeletedAt         gorm.DeletedAt `gorm:"column:deleted_at;type:datetime"`
}
