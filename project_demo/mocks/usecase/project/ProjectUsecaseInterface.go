// Code generated by mockery v2.14.0. DO NOT EDIT.

package mocks

import (
	entity "main/entity"

	mock "github.com/stretchr/testify/mock"
)

// ProjectUsecaseInterface is an autogenerated mock type for the ProjectUsecaseInterface type
type ProjectUsecaseInterface struct {
	mock.Mock
}

// CreateProject provides a mock function with given fields: project
func (_m *ProjectUsecaseInterface) CreateProject(project *entity.Project) error {
	ret := _m.Called(project)

	var r0 error
	if rf, ok := ret.Get(0).(func(*entity.Project) error); ok {
		r0 = rf(project)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteProject provides a mock function with given fields: id
func (_m *ProjectUsecaseInterface) DeleteProject(id int) error {
	ret := _m.Called(id)

	var r0 error
	if rf, ok := ret.Get(0).(func(int) error); ok {
		r0 = rf(id)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetListProjects provides a mock function with given fields:
func (_m *ProjectUsecaseInterface) GetListProjects() ([]entity.Project, error) {
	ret := _m.Called()

	var r0 []entity.Project
	if rf, ok := ret.Get(0).(func() []entity.Project); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]entity.Project)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// TakeProject provides a mock function with given fields: id
func (_m *ProjectUsecaseInterface) TakeProject(id int) (*entity.Project, error) {
	ret := _m.Called(id)

	var r0 *entity.Project
	if rf, ok := ret.Get(0).(func(int) *entity.Project); ok {
		r0 = rf(id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*entity.Project)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(int) error); ok {
		r1 = rf(id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UpdateProject provides a mock function with given fields: project
func (_m *ProjectUsecaseInterface) UpdateProject(project *entity.Project) error {
	ret := _m.Called(project)

	var r0 error
	if rf, ok := ret.Get(0).(func(*entity.Project) error); ok {
		r0 = rf(project)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

type mockConstructorTestingTNewProjectUsecaseInterface interface {
	mock.TestingT
	Cleanup(func())
}

// NewProjectUsecaseInterface creates a new instance of ProjectUsecaseInterface. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewProjectUsecaseInterface(t mockConstructorTestingTNewProjectUsecaseInterface) *ProjectUsecaseInterface {
	mock := &ProjectUsecaseInterface{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
