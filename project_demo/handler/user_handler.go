package handler

import (
	"bytes"
	"encoding/csv"
	"errors"
	"main/dto"
	validate "main/pkg/validate/user"
	repository "main/repository/user"
	usecase "main/usecase/user"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

type UserHandler struct {
	UserUsecase usecase.UserUsecaseInterface
}

func NewUserHandler(db *gorm.DB) *UserHandler {
	userRepo := repository.NewUserRepository(db)
	userUsecase := usecase.NewUserUsecase(userRepo)

	return &UserHandler{
		UserUsecase: userUsecase,
	}
}

func (userHandler *UserHandler) Login(ctx *gin.Context) {
	userLoginRequest := &dto.UserLoginRequest{}
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("Password", validate.Password)
	}

	if err := ctx.ShouldBindJSON(userLoginRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userToken, err := userHandler.UserUsecase.GetUserTokenByEmail(userLoginRequest)

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"token": userToken})
}

func (userHandler *UserHandler) Register(ctx *gin.Context) {
	userRegisterRequest := &dto.UserRequest{}
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("Password", validate.Password)
	}

	if err := ctx.ShouldBindJSON(userRegisterRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if _, err := userHandler.UserUsecase.CreateUser(userRegisterRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "registration success"})
}

func (userHandler *UserHandler) CreateUser(ctx *gin.Context) {
	userRequest := &dto.UserRequest{}
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("Password", validate.Password)
	}

	if err := ctx.BindJSON(userRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userResponse, err := userHandler.UserUsecase.CreateUser(userRequest)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.IndentedJSON(http.StatusCreated, userResponse)
}

func (userHandler *UserHandler) GetUserProjects(ctx *gin.Context) {
	userID, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "id is not a number"})
		return
	}

	userProjects, err := userHandler.UserUsecase.GetUserProjects(userID)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.IndentedJSON(http.StatusOK, userProjects)
}

func (userHandler *UserHandler) ExportUserProjectsReport(ctx *gin.Context) {
	userID, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "id is not a number"})
		return
	}

	userProjects, err := userHandler.UserUsecase.GetUserProjects(userID)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var buf bytes.Buffer
	w := csv.NewWriter(&buf)
	title := []string{
		"id",
		"name",
		"category",
		"project_spend",
		"project_variance",
		"revenue_recognised",
	}

	if err := w.Write(title); err != nil {
		ctx.JSON(http.StatusBadRequest, errors.New("error exporting to csv"))
		return
	}

	for _, project := range userProjects.Projects {
		row := []string{
			strconv.Itoa(int(project.ID)),
			project.Name,
			project.Category,
			strconv.Itoa(project.ProjectSpend),
			strconv.Itoa(project.ProjectVariance),
			strconv.Itoa(project.RevenueRecognised),
		}
		if err := w.Write(row); err != nil {
			ctx.JSON(http.StatusInternalServerError, errors.New("error exporting to csv"))
			return
		}
	}

	w.Flush()
	ctx.Writer.Header().Set("Content-Type", "text/csv")
	csvName := "export_user_projects_report_" + time.Now().Format("20060102")
	ctx.Writer.Header().Set("Content-Disposition", "attachment;filename="+csvName+".csv")
	ctx.Writer.Write(buf.Bytes())
}
