package handler

import (
	"main/dto"
	validate "main/pkg/validate/project"
	repository "main/repository/project"
	usecase "main/usecase/project"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

type ProjectHandler struct {
	ProjectUsecase usecase.ProjectUsecaseInterface
}

func NewProjectHandler(db *gorm.DB) *ProjectHandler {
	projectRepo := repository.NewProjectRepository(db)
	projectUsecase := usecase.NewProjectUsecase(projectRepo)

	return &ProjectHandler{
		ProjectUsecase: projectUsecase,
	}
}

func (projectHandler *ProjectHandler) CreateProject(ctx *gin.Context) {
	projectRequest := &dto.ProjectRequest{}
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("Enum", validate.Enum)
		v.RegisterValidation("Date", validate.Date)
		v.RegisterValidation("StartDate", validate.StartDate)
	}

	if err := ctx.ShouldBindJSON(projectRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	projectResponse, err := projectHandler.ProjectUsecase.CreateProject(projectRequest)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.IndentedJSON(http.StatusCreated, projectResponse)
}

func (projectHandler *ProjectHandler) TakeProject(ctx *gin.Context) {
	userID, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "id is not a number"})
		return
	}

	project, err := projectHandler.ProjectUsecase.TakeProject(userID)
	switch err {
	case nil:
		ctx.IndentedJSON(http.StatusOK, project)
	case gorm.ErrRecordNotFound:
		ctx.JSON(http.StatusNotFound, gin.H{"error": "record not found"})
	default:
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
}

func (projectHandler *ProjectHandler) DeleteProject(ctx *gin.Context) {
	userID, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "id is not a number"})
		return
	}

	project, err := projectHandler.ProjectUsecase.DeleteProject(userID)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.IndentedJSON(http.StatusOK, project)
}

func (projectHandler *ProjectHandler) UpdateProject(ctx *gin.Context) {
	projectRequest := &dto.ProjectRequest{}
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("Enum", validate.Enum)
		v.RegisterValidation("Date", validate.Date)
		v.RegisterValidation("StartDate", validate.StartDate)
	}

	if err := ctx.ShouldBindJSON(&projectRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	projectID, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "id is not a number"})
		return
	}

	projectResponse, err := projectHandler.ProjectUsecase.UpdateProject(projectID, projectRequest)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.IndentedJSON(http.StatusCreated, projectResponse)
}

func (projectHandler *ProjectHandler) GetListProjects(ctx *gin.Context) {
	projects, err := projectHandler.ProjectUsecase.GetListProjects()
	if err == gorm.ErrRecordNotFound {
		ctx.JSON(http.StatusNoContent, projects)
		return
	} else if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.IndentedJSON(http.StatusOK, projects)
}
