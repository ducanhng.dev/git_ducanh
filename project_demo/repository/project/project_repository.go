package repository

import (
	"main/entity"

	_ "github.com/go-sql-driver/mysql"
	"gorm.io/gorm"
)

type ProjectRepositoryInterface interface {
	CreateProject(project *entity.Project) error
	TakeProject(id int) (*entity.Project, error)
	DeleteProject(id int) error
	UpdateProject(project *entity.Project) error
	GetListProjects() ([]entity.Project, error)
}

type ProjectRepository struct {
	db *gorm.DB
}

func NewProjectRepository(db *gorm.DB) ProjectRepositoryInterface {
	return &ProjectRepository{
		db: db,
	}
}

func (projectRepository *ProjectRepository) CreateProject(project *entity.Project) error {
	return projectRepository.db.Create(&project).Error
}

func (projectRepository *ProjectRepository) TakeProject(id int) (*entity.Project, error) {
	var project *entity.Project
	return project, projectRepository.db.Take(&project, id).Error
}

func (projectRepository *ProjectRepository) DeleteProject(id int) error {
	var project *entity.Project
	return projectRepository.db.Delete(&project, id).Error
}

func (projectRepository *ProjectRepository) UpdateProject(project *entity.Project) error {
	return projectRepository.db.Where("id = ?", project.ID).Updates(&project).Error
}

func (projectRepository *ProjectRepository) GetListProjects() ([]entity.Project, error) {
	var projects []entity.Project
	return projects, projectRepository.db.Find(&projects).Error
}
