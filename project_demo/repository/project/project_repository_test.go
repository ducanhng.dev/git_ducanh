package repository

import (
	"errors"
	"main/entity"
	mocks "main/mocks/repository/project"
	"reflect"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
)

func TestProjectRepository_CreateProject(t *testing.T) {
	type args struct {
		project *entity.Project
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		// TODO: Add test cases.
		{
			name: "success_create_project",
			args: args{
				project: &entity.Project{
					Name:            "Test",
					Category:        "Test",
					ProjectSpend:    99,
					ProjectVariance: 99,
				},
			},
			wantErr: nil,
		},
		{
			name: "failed_create_project",
			args: args{
				project: &entity.Project{
					Name:            "Test",
					Category:        "Test",
					ProjectSpend:    99,
					ProjectVariance: 99,
				},
			},
			wantErr: errors.New("project is not created"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &mocks.ProjectRepositoryInterface{}
			if tt.wantErr == nil {
				repo.On("CreateProject", tt.args.project).Return(nil)
			} else {
				repo.On("CreateProject", tt.args.project).Return(errors.New("project is not created"))
			}
			if err := repo.CreateProject(tt.args.project); err != tt.wantErr {
				assert.Error(t, err)
				assert.Equal(t, tt.wantErr.Error(), err.Error())
				return
			} else {
				assert.Nil(t, err)
				return
			}
		})
	}
}

func TestProjectRepository_TakeProject(t *testing.T) {
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		args    args
		want    *entity.Project
		wantErr error
	}{
		{
			name: "success_take_project",
			args: args{
				id: 0,
			},
			want: &entity.Project{
				Name:              "Test",
				Category:          "Test",
				ProjectSpend:      99,
				ProjectVariance:   99,
				RevenueRecognised: 99,
			},
			wantErr: nil,
		},
		{
			name: "failed_take_project",
			args: args{
				id: 0,
			},
			want:    &entity.Project{},
			wantErr: errors.New("failed to take project"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &mocks.ProjectRepositoryInterface{}
			if tt.wantErr == nil {
				repo.On("TakeProject", tt.args.id).Return(tt.want, nil)
			} else {
				repo.On("TakeProject", tt.args.id).Return(tt.want, errors.New("failed to take project"))
			}
			got, err := repo.TakeProject(tt.args.id)
			if err != tt.wantErr {
				assert.Error(t, err)
				assert.Equal(t, err.Error(), tt.wantErr.Error())
				return
			} else {
				assert.Nil(t, err)
			}
			if !reflect.DeepEqual(got, tt.want) {
				assert.Error(t, err)
				return
			}
		})
	}
}

func TestProjectRepository_DeleteProject(t *testing.T) {
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		// TODO: Add test cases.
		{
			name: "success_delete_project",
			args: args{
				id: 0,
			},
			wantErr: nil,
		},
		{
			name: "failed_delete_project",
			args: args{
				id: 0,
			},
			wantErr: errors.New("project is not deleted"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &mocks.ProjectRepositoryInterface{}
			if tt.wantErr == nil {
				repo.On("DeleteProject", tt.args.id).Return(nil)
			} else {
				repo.On("DeleteProject", tt.args.id).Return(errors.New("project is not deleted"))
			}
			if err := repo.DeleteProject(tt.args.id); err != tt.wantErr {
				assert.Error(t, err)
				assert.Equal(t, err.Error(), tt.wantErr.Error())
				return
			} else {
				assert.Nil(t, err)
			}
		})
	}
}

func TestProjectRepository_UpdateProject(t *testing.T) {
	type args struct {
		project *entity.Project
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		// TODO: Add test cases.
		{
			name: "success_update_project",
			args: args{
				project: &entity.Project{
					ID:       3,
					Name:     "Test Update",
					Category: "Test Update",
				},
			},
			wantErr: nil,
		},
		{
			name: "failed_update_project",
			args: args{
				project: &entity.Project{
					ID:       3,
					Name:     "Test Update",
					Category: "Test Update",
				},
			},
			wantErr: errors.New("project is not updated"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &mocks.ProjectRepositoryInterface{}
			if tt.wantErr == nil {
				repo.On("UpdateProject", tt.args.project).Return(nil)
			} else {
				repo.On("UpdateProject", tt.args.project).Return(errors.New("project is not updated"))
			}
			if err := repo.UpdateProject(tt.args.project); err != tt.wantErr {
				assert.Error(t, err)
				assert.Equal(t, err.Error(), tt.wantErr.Error())
				return
			} else {
				assert.Nil(t, err)
			}
		})
	}
}
