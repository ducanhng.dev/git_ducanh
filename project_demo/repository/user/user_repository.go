package repository

import (
	"main/entity"

	_ "github.com/go-sql-driver/mysql"
	"gorm.io/gorm"
)

type UserRepositoryInterface interface {
	GetUserByEmail(user *entity.User) (*entity.User, error)
	CreateUser(user *entity.User) error
	TakeUser(id int) (*entity.User, error)
	GetUserProjects(id int) (*entity.User, error)
}

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepositoryInterface {
	return &UserRepository{
		db: db,
	}
}

func (userRepository *UserRepository) CreateUser(user *entity.User) error {
	return userRepository.db.Create(&user).Error
}

func (userRepository *UserRepository) TakeUser(id int) (*entity.User, error) {
	var user *entity.User
	return user, userRepository.db.Take(&user, id).Error
}

func (userRepository *UserRepository) GetUserProjects(id int) (*entity.User, error) {
	var user *entity.User
	return user, userRepository.db.Preload("Projects").Take(&user, id).Error
}

func (userRepository *UserRepository) GetUserByEmail(user *entity.User) (*entity.User, error) {
	return user, userRepository.db.Where("email = ?", user.Email).Take(&user).Error
}
