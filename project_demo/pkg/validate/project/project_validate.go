package validate

import (
	"main/pkg/constants"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
)

func Enum(fl validator.FieldLevel) bool {
	enumString := fl.Param()
	value := fl.Field().String()
	enumSlice := strings.Split(enumString, "_")
	for _, v := range enumSlice {
		if value == v {
			return true
		}
	}
	return false
}

func Date(fl validator.FieldLevel) bool {
	dateStart := fl.Parent().FieldByName("ProjectStartedAt").String()
	start, err := time.Parse(constants.DateFormat, dateStart)
	if err != nil {
		return false
	}
	dateEnd := fl.Parent().FieldByName("ProjectEndedAt").String()
	end, err := time.Parse(constants.DateFormat, dateEnd)
	if err != nil {
		return false
	}
	return start.Before(end)
}

func StartDate(fl validator.FieldLevel) bool {
	dateStart := fl.Parent().FieldByName("ProjectStartedAt").String()
	start, err := time.Parse(constants.DateFormat, dateStart)
	if err != nil {
		return false
	}
	return start.After(time.Now())
}
