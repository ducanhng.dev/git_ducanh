package validate

import (
	"regexp"

	"github.com/go-playground/validator/v10"
)

func isValid(s string) bool {
	hasMinLength := regexp.MustCompile(`.{8,10}`)
	hasLower := regexp.MustCompile(`[a-z]`)
	hasUpper := regexp.MustCompile(`[A-Z]`)
	hasDigit := regexp.MustCompile(`[0-9]`)
	hasSymbol := regexp.MustCompile(`[!@#$%^&*()]`)
	hasAll := regexp.MustCompile(`^[0-9a-zA-Z!@#$%^&*()]*$`)
	return hasDigit.MatchString(s) && hasLower.MatchString(s) && hasUpper.MatchString(s) && hasSymbol.MatchString(s) && hasAll.MatchString(s) && hasMinLength.MatchString(s)
}

func Password(fl validator.FieldLevel) bool {
	password := fl.Field().String()
	return isValid(password)
}
