package main

import (
	"log"
	"main/handler"
	"main/pkg/database"
	"main/pkg/middleware"

	"github.com/gin-gonic/gin"
)

func main() {
	db, err := database.Connect()
	if err != nil {
		log.Fatalln("mysql", "err", err)
	}

	router := gin.Default()

	userHandler := handler.NewUserHandler(db)
	router.POST("/login", userHandler.Login)
	router.POST("/register", userHandler.Register)
	userGroup := router.Group("/api/users", middleware.CheckTokenValid())
	{
		userGroup.POST("/create", userHandler.CreateUser)
		userGroup.GET("/:id/projects", userHandler.GetUserProjects)
		userGroup.GET("/:id/export_projects_report", userHandler.ExportUserProjectsReport)
	}

	projectHandler := handler.NewProjectHandler(db)
	projectGroup := router.Group("/api/projects", middleware.CheckTokenValid())
	{
		projectGroup.POST("/create", projectHandler.CreateProject)
		projectGroup.GET("/:id", projectHandler.TakeProject)
		projectGroup.PATCH("/:id", projectHandler.UpdateProject)
		projectGroup.DELETE("/:id", projectHandler.DeleteProject)
		projectGroup.GET("/", projectHandler.GetListProjects)
	}

	router.Run(":8080")
}
