package usecase

import (
	"errors"
	"main/dto"
	"main/entity"
	"main/pkg/auth"
	repository "main/repository/user"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserUsecaseInterface interface {
	GetUserTokenByEmail(user *dto.UserLoginRequest) (string, error)
	CreateUser(user *dto.UserRequest) (*dto.UserResponse, error)
	TakeUser(id int) (*entity.User, error)
	GetUserProjects(id int) (*dto.UserProjectsResponse, error)
}

type UserUsecase struct {
	userRepo repository.UserRepositoryInterface
}

func NewUserUsecase(userRepo repository.UserRepositoryInterface) UserUsecaseInterface {
	return &UserUsecase{
		userRepo,
	}
}

func HashPassword(password string) (string, error) {
	hashPassword, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		return "", err
	}
	return string(hashPassword), nil
}

func (userUseCase *UserUsecase) GetUserTokenByEmail(userLoginRequest *dto.UserLoginRequest) (string, error) {
	user, err := userUseCase.userRepo.GetUserByEmail(&entity.User{Email: userLoginRequest.Email})
	if err != nil {
		return "", err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(userLoginRequest.Password)); err != nil {
		return "", err
	}

	token, err := auth.GenerateJWT(user.ID)

	if err != nil {
		return "", err
	}

	return token, nil
}

func (userUsecase *UserUsecase) CreateUser(userRequest *dto.UserRequest) (*dto.UserResponse, error) {
	pwdHashed, err := HashPassword(userRequest.Password)
	if err != nil {
		return nil, err
	}

	user := &entity.User{
		FirstName: userRequest.FirstName,
		LastName:  userRequest.LastName,
		Email:     userRequest.Email,
		Password:  pwdHashed,
	}
	if err := userUsecase.userRepo.CreateUser(user); err != nil {
		return nil, err
	}

	return &dto.UserResponse{
		FirstName: userRequest.FirstName,
		LastName:  userRequest.LastName,
		Email:     userRequest.Email,
		Password:  userRequest.Password,
	}, nil
}

func (userUsecase *UserUsecase) TakeUser(id int) (*entity.User, error) {
	return userUsecase.userRepo.TakeUser(id)
}

func (userUsecase *UserUsecase) GetUserProjects(id int) (*dto.UserProjectsResponse, error) {
	_, err := userUsecase.TakeUser(id)
	if err == gorm.ErrRecordNotFound {
		return nil, errors.New("records not found")
	} else if err != nil {
		return nil, err
	}

	userProject, err := userUsecase.userRepo.GetUserProjects(id)
	if err != nil {
		return nil, err
	}

	projects := []dto.ProjectResponse{}
	for _, userProject := range userProject.Projects {
		projects = append(projects, dto.ProjectResponse{
			ID:               userProject.ID,
			Name:             userProject.Name,
			Category:         userProject.Category,
			ProjectSpend:     userProject.ProjectSpend,
			ProjectVariance:  userProject.ProjectVariance,
			ProjectStartedAt: userProject.ProjectStartedAt,
			ProjectEndedAt:   *userProject.ProjectEndedAt,
		})
	}

	return &dto.UserProjectsResponse{
		ID:        userProject.ID,
		FirstName: userProject.FirstName,
		LastName:  userProject.LastName,
		Email:     userProject.Email,
		Password:  userProject.Password,
		CreatedAt: userProject.CreatedAt,
		UpdatedAt: userProject.UpdatedAt,
		Projects:  projects,
	}, nil
}
