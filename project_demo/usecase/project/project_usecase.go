package usecase

import (
	"errors"
	"main/dto"
	"main/entity"
	"main/pkg/constants"
	repository "main/repository/project"
	"time"

	"gorm.io/gorm"
)

type ProjectUsecaseInterface interface {
	CreateProject(project *dto.ProjectRequest) (*dto.ProjectResponse, error)
	TakeProject(id int) (*dto.ProjectDetailResponse, error)
	DeleteProject(id int) (*dto.ProjectResponse, error)
	UpdateProject(id int, projectRequest *dto.ProjectRequest) (*dto.ProjectResponse, error)
	GetListProjects() ([]dto.ProjectResponse, error)
}

type ProjectUsecase struct {
	ProjectRepo repository.ProjectRepositoryInterface
}

func NewProjectUsecase(ProjectRepo repository.ProjectRepositoryInterface) ProjectUsecaseInterface {
	return &ProjectUsecase{
		ProjectRepo,
	}
}

func (projectUsecase *ProjectUsecase) CreateProject(projectRequest *dto.ProjectRequest) (*dto.ProjectResponse, error) {
	dateStart, err := time.Parse(constants.DateFormat, projectRequest.ProjectStartedAt)
	if err != nil {
		return nil, err
	}
	dateEnd, err := time.Parse(constants.DateFormat, projectRequest.ProjectEndedAt)
	if err != nil {
		return nil, err
	}

	project := &entity.Project{
		Name:             projectRequest.Name,
		Category:         projectRequest.Category,
		ProjectSpend:     projectRequest.ProjectSpend,
		ProjectVariance:  projectRequest.ProjectVariance,
		ProjectStartedAt: dateStart,
		ProjectEndedAt:   &dateEnd,
	}

	if err := projectUsecase.ProjectRepo.CreateProject(project); err != nil {
		return nil, err
	}

	return &dto.ProjectResponse{
		Name:             projectRequest.Name,
		Category:         projectRequest.Category,
		ProjectSpend:     projectRequest.ProjectSpend,
		ProjectVariance:  projectRequest.ProjectVariance,
		ProjectStartedAt: dateStart,
		ProjectEndedAt:   dateEnd,
	}, nil
}

func (projectUsecase *ProjectUsecase) TakeProject(id int) (*dto.ProjectDetailResponse, error) {
	project, err := projectUsecase.ProjectRepo.TakeProject(id)
	if err != nil {
		return nil, err
	}

	return &dto.ProjectDetailResponse{
		ID:                project.ID,
		Name:              project.Name,
		Category:          project.Category,
		ProjectSpend:      project.ProjectSpend,
		ProjectVariance:   project.ProjectVariance,
		RevenueRecognised: project.RevenueRecognised,
		ProjectStartedAt:  project.ProjectStartedAt,
		ProjectEndedAt:    *project.ProjectEndedAt,
		CreatedAt:         project.CreatedAt,
		UpdatedAt:         project.UpdatedAt,
		DeletedAt:         project.DeletedAt.Time,
	}, nil
}

func (projectUsecase *ProjectUsecase) DeleteProject(id int) (*dto.ProjectResponse, error) {
	project, err := projectUsecase.TakeProject(id)
	if err == gorm.ErrRecordNotFound {
		return nil, errors.New("record not found")
	} else if err != nil {
		return nil, err
	}

	if err := projectUsecase.ProjectRepo.DeleteProject(id); err != nil {
		return nil, err
	}

	return &dto.ProjectResponse{
		ID:               project.ID,
		Name:             project.Name,
		Category:         project.Category,
		ProjectSpend:     project.ProjectSpend,
		ProjectVariance:  project.ProjectVariance,
		ProjectStartedAt: project.ProjectStartedAt,
		ProjectEndedAt:   project.ProjectEndedAt,
	}, nil
}

func (projectUsecase *ProjectUsecase) UpdateProject(id int, projectRequest *dto.ProjectRequest) (*dto.ProjectResponse, error) {
	_, err := projectUsecase.TakeProject(id)
	if err == gorm.ErrRecordNotFound {
		return nil, errors.New("record not found")
	} else if err != nil {
		return nil, err
	}

	dateStart, err := time.Parse(constants.DateFormat, projectRequest.ProjectStartedAt)
	if err != nil {
		return nil, err
	}
	dateEnd, err := time.Parse(constants.DateFormat, projectRequest.ProjectEndedAt)
	if err != nil {
		return nil, err
	}

	project := &entity.Project{
		ID:               uint(id),
		Name:             projectRequest.Name,
		Category:         projectRequest.Category,
		ProjectSpend:     projectRequest.ProjectSpend,
		ProjectVariance:  projectRequest.ProjectVariance,
		ProjectStartedAt: dateStart,
		ProjectEndedAt:   &dateEnd,
	}

	if err := projectUsecase.ProjectRepo.UpdateProject(project); err != nil {
		return nil, err
	}

	return &dto.ProjectResponse{
		Name:             projectRequest.Name,
		Category:         projectRequest.Category,
		ProjectSpend:     projectRequest.ProjectSpend,
		ProjectVariance:  projectRequest.ProjectVariance,
		ProjectStartedAt: dateStart,
		ProjectEndedAt:   dateEnd,
	}, nil
}
func (projectUsecase *ProjectUsecase) GetListProjects() ([]dto.ProjectResponse, error) {
	projects, err := projectUsecase.ProjectRepo.GetListProjects()
	if err != nil {
		return nil, err
	}

	projectsResponse := []dto.ProjectResponse{}
	for _, project := range projects {
		projectsResponse = append(projectsResponse, dto.ProjectResponse{
			ID:               project.ID,
			Name:             project.Name,
			Category:         project.Category,
			ProjectSpend:     project.ProjectSpend,
			ProjectVariance:  project.ProjectVariance,
			ProjectStartedAt: project.ProjectStartedAt,
			ProjectEndedAt:   *project.ProjectEndedAt,
		})
	}

	return projectsResponse, nil
}
