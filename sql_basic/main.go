package main

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db, err := sql.Open("mysql", "root:root@tcp(localhost:3306)/root")
	if err != nil {
		panic(fmt.Errorf("Connection fail! %v", err))
	}

	db.SetConnMaxIdleTime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	_, err = db.Exec(`
		CREATE TABLE sample_table (
			id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			column_sample varchar(255) NOT NULL
		);
	`)

	if err != nil {
		panic(fmt.Errorf("Executions fail! %v", err))
	}

	fmt.Println("Connection success!")
}
