-- 1
select categories.id, categories.name, categories.description, count(items.id) as items_count 
from categories inner join items 
on categories.id = items.category_id 
group by categories.id;

--2
select categories.id, categories.name, categories.description, sum(items.amount) as sum_amount
from categories inner join items 
on categories.id = items.category_id 
group by categories.id;

--3 
select distinct categories.id, categories.name, categories.description
from categories inner join items 
on categories.id = items.category_id 
where items.amount > 40;

--4
delete from categories 
where id not in (
    select cid from (
        select distinct items.category_id as cid 
        from items inner join categories 
        on items.category_id = categories.id
    ) as c);