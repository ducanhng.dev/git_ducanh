package main

import (
	"fmt"
	"regexp"
	"strings"
	"time"
)

type Persons struct {
	name         string
	birthdayYear int
	age          int
	email        string
	phone        interface{}
}

type PersonsInterface interface {
	setName(name string)
	setBirthdayYear(birthdayYear int)
	setEmail(email string)
	setPhone(phone interface{})
}

func PersonsInterfaceImplement() PersonsInterface {
	return &Persons{}
}

func (p *Persons) setName(name string) {
	reg, err := regexp.Compile("^[A-Z]")
	if err != nil {
		fmt.Println(err)
	} else {
		if flag := reg.MatchString(name); flag == true {
			p.name = name
		} else {
			fmt.Println("Tên không đúng cú pháp")
		}
	}
}

func (p *Persons) setBirthdayYear(birthdayYear int) {
	t := time.Now()
	year := t.Year()
	if birthdayYear < 1900 {
		fmt.Println("Năm sinh phải lớn hơn 1900")
	} else {
		p.birthdayYear = birthdayYear
		p.age = year - birthdayYear
	}
}

func (p *Persons) setEmail(email string) {
	reg, err := regexp.Compile("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$")
	if err != nil {
		fmt.Println(err)
	} else {
		if flag := reg.MatchString(email); flag == true {
			p.email = email
		} else {
			fmt.Println("Email không đúng cú pháp")
		}
	}
}

func (p *Persons) setPhone(phone interface{}) {
	strPhone := fmt.Sprintf("%v", phone)
	switch phone.(type) {
	case int:
		strPhone = "0" + strPhone
		splitValue := strings.Split(strPhone, "")
		if len(splitValue) < 10 || len(splitValue) > 11 {
			fmt.Println("Số điện thoại trong khoảng 10-11 số!")
		} else {
			p.phone = strPhone
		}
	case string:
		splitValue := strings.Split(strPhone, "")
		if splitValue[0] != "+" {
			fmt.Println("Số điện thoại không đúng định dạng!")
		} else {
			if len(splitValue) < 11 || len(splitValue) > 12 {
				fmt.Println("Số điện thoại trong khoảng 10-11 số!")
			} else {
				p.phone = strPhone
			}
		}
	default:
		fmt.Println("Nothing")
	}
}

func main() {
	p := Persons{}
	p.name = "Nguyen Duc Anh"
	p.age = 21
	p.birthdayYear = 2001
	p.email = "ducanhng.dev@gmail.com"
	p.phone = "0857281729"
	fmt.Println(p)

	var p2 PersonsInterface
	p2 = PersonsInterfaceImplement()
	p2.setName("anh")
	p2.setBirthdayYear(1888)
	p2.setEmail("ducanhng.dev")
	p2.setPhone(87951962899)

	var pI PersonsInterface
	pI = PersonsInterfaceImplement()
	pI.setName("Anh")
	pI.setBirthdayYear(2001)
	pI.setEmail("ducanhng.dev@gmail.com")
	pI.setPhone("+84879519628")
	fmt.Println(pI)
}
