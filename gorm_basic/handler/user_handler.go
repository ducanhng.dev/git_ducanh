package handler

import (
	"main/dtos"
	"main/repository"
	"main/usecase"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type UserHandler struct {
	UserUsecase    usecase.UserUsecaseInterface
	ProjectUsecase usecase.ProjectUsecaseInterface
}

func NewUserHandler(dbConn *gorm.DB) *UserHandler {
	userRepo := repository.NewUserRepository(dbConn)
	userUsecase := usecase.NewUserUsecase(userRepo)

	projectRepo := repository.NewProjectRepository(dbConn)
	projectUcase := usecase.NewProjectUsecase(projectRepo)
	return &UserHandler{
		ProjectUsecase: projectUcase,
		UserUsecase:    userUsecase,
	}
}

func (h *UserHandler) CreateUser(c *gin.Context) {
	userRequest := dtos.UserRequest{}
	if err := c.BindJSON(&userRequest); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if err := h.UserUsecase.CreateUser(userRequest); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.IndentedJSON(http.StatusCreated, dtos.UserResponse{
		FirstName: userRequest.FirstName,
		LastName:  userRequest.LastName,
		Email:     userRequest.Email,
		Password:  userRequest.Password,
	})
}

func (h *UserHandler) TakeUser(c *gin.Context) {
	userID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "id is not a number"})
		return
	}
	user, err := h.UserUsecase.TakeUser(userID)
	switch err {
	case gorm.ErrRecordNotFound:
		c.JSON(http.StatusNotFound, gin.H{"error": "record not found"})
	default:
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	c.IndentedJSON(http.StatusOK, user)
}

func (h *UserHandler) GetUserProjects(c *gin.Context) {
	userID, err := strconv.Atoi(c.Param("user_id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "id is not a number"})
		return
	}
	userProjects, err := h.UserUsecase.GetUserPreloadProjects(userID)
	switch err {
	case gorm.ErrRecordNotFound:
		c.JSON(http.StatusNotFound, gin.H{"error": "record not found"})
	default:
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	projectsResponse := []dtos.ProjectsOfUserResponse{}
	for _, project := range userProjects.Projects {
		projectsResponse = append(projectsResponse, dtos.ProjectsOfUserResponse{
			ID:               project.ID,
			Name:             project.Name,
			ProjectStartedAt: project.ProjectStartedAt,
			ProjectEndedAt:   project.ProjectEndedAt,
		})
	}
	c.IndentedJSON(http.StatusOK, dtos.UserProjectsResponse{
		ID:        userProjects.ID,
		FirstName: userProjects.FirstName,
		LastName:  userProjects.LastName,
		Email:     userProjects.Email,
		Password:  userProjects.Password,
		CreatedAt: userProjects.CreatedAt,
		UpdatedAt: userProjects.UpdatedAt,
		Projects:  projectsResponse,
	})
}
