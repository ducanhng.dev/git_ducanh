package handler

import (
	"main/dtos"
	"main/entity"
	"main/repository"
	"main/usecase"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

const dateFormat = "2006-01-02"

type ProjectHandler struct {
	ProjectUsecase usecase.ProjectUsecaseInterface
	UserUsecase    usecase.UserUsecaseInterface
}

func NewProjectHandler(dbConn *gorm.DB) *ProjectHandler {
	projectRepo := repository.NewProjectRepository(dbConn)
	projectUsecase := usecase.NewProjectUsecase(projectRepo)

	userRepo := repository.NewUserRepository(dbConn)
	userUsecase := usecase.NewUserUsecase(userRepo)

	return &ProjectHandler{
		UserUsecase:    userUsecase,
		ProjectUsecase: projectUsecase,
	}
}

func (h *ProjectHandler) CreateProject(c *gin.Context) {
	projectRequest := dtos.ProjectRequest{}
	if err := c.BindJSON(&projectRequest); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	_, err := h.UserUsecase.TakeUser(int(projectRequest.UserID))
	switch err {
	case gorm.ErrRecordNotFound:
		c.JSON(http.StatusNotFound, gin.H{"error": "record not found"})
	default:
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	dateStart, err := time.Parse(dateFormat, projectRequest.ProjectStartedAt)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	dateEnd, err := time.Parse(dateFormat, projectRequest.ProjectEndedAt)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	project := &entity.Project{
		Name:             projectRequest.Name,
		ProjectStartedAt: dateStart,
		ProjectEndedAt:   dateEnd,
		UserID:           projectRequest.UserID,
	}
	if err = h.ProjectUsecase.CreateProject(project); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.IndentedJSON(http.StatusCreated, dtos.ProjectCreateResponse{
		Name:             projectRequest.Name,
		ProjectStartedAt: dateStart,
		ProjectEndedAt:   dateEnd,
		UserID:           projectRequest.UserID,
	})
}

func (h *ProjectHandler) GetProjectByUserID(c *gin.Context) {
	userID, err := strconv.Atoi(c.Param("user_id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "id is not a number"})
		return
	}
	projects, err := h.ProjectUsecase.GetProjectByUserID(userID)
	switch err {
	case gorm.ErrRecordNotFound:
		c.JSON(http.StatusNotFound, gin.H{"error": "record not found"})
	default:
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	c.IndentedJSON(http.StatusOK, projects)
}
