package usecase

import (
	"main/dtos"
	"main/entity"
	"main/repository"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type UserUsecaseInterface interface {
	CreateUser(userRequest dtos.UserRequest) error
	TakeUser(id int) (*entity.User, error)
	GetUserPreloadProjects(id int) (*entity.User, error)
}

type UserUsecase struct {
	userRepo repository.UserRepositoryInterface
}

func NewUserUsecase(userRepo repository.UserRepositoryInterface) UserUsecaseInterface {
	return &UserUsecase{
		userRepo,
	}
}

func HashPassword(password string) (string, error) {
	hashPassword, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		return "", err
	}
	return string(hashPassword), nil
}

func (uc *UserUsecase) CreateUser(userRequest dtos.UserRequest) error {
	pwdHashed, err := HashPassword(userRequest.Password)
	if err != nil {
		return err
	}
	user := &entity.User{
		FirstName: userRequest.FirstName,
		LastName:  userRequest.LastName,
		Email:     userRequest.Email,
		Password:  pwdHashed,
		CreatedAt: time.Time{},
	}
	return uc.userRepo.CreateUser(user)
}

func (uc *UserUsecase) TakeUser(id int) (*entity.User, error) {
	return uc.userRepo.TakeUserByID(id)
}

func (uc *UserUsecase) GetUserPreloadProjects(id int) (*entity.User, error) {
	return uc.userRepo.GetUserPreloadProjects(id)
}
