package usecase

import (
	"main/entity"
	"main/repository"
)

type ProjectUsecaseInterface interface {
	CreateProject(project *entity.Project) error
	GetProjectByUserID(id int) ([]entity.Project, error)
}

type ProjectUsecase struct {
	ProjectRepo repository.ProjectRepositoryInterface
}

func NewProjectUsecase(ProjectRepo repository.ProjectRepositoryInterface) ProjectUsecaseInterface {
	return &ProjectUsecase{
		ProjectRepo,
	}
}

func (uc *ProjectUsecase) CreateProject(project *entity.Project) error {
	return uc.ProjectRepo.CreateProject(project)
}

func (uc *ProjectUsecase) GetProjectByUserID(id int) ([]entity.Project, error) {
	return uc.ProjectRepo.GetProjectByUserID(id)
}
