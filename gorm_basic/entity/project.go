package entity

import (
	"time"
)

type Project struct {
	ID               uint      `gorm:"column:id;primarykey"`
	Name             string    `gorm:"column:name"`
	ProjectStartedAt time.Time `gorm:"column:project_started_at"`
	ProjectEndedAt   time.Time `gorm:"column:project_ended_at"`
	UserID           int64     `gorm:"column:user_id"`
	User             User      `gorm:"foreignKey:UserID"`
}
