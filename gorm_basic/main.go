package main

import (
	"log"
	"main/db"
	"main/entity"
	"main/handler"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func Logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		log.Printf("call to endpoints user")
		c.Next()
	}
}

func main() {
	db, err := db.Connect()
	if err != nil {
		log.Fatalln("mysql", "err", err)
	}

	db.AutoMigrate(&entity.Project{})
	db.AutoMigrate(&entity.User{})

	router := gin.Default()
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "Hellooo",
		})
	})
	userHandler := handler.NewUserHandler(db)
	userGroup := router.Group("/user", Logger())
	{
		userGroup.POST("/create", userHandler.CreateUser)
		userGroup.GET("/:id", userHandler.TakeUser)
	}
	projectHandler := handler.NewProjectHandler(db)
	projectGroup := router.Group("/user_project", Logger())
	{
		projectGroup.POST("/create", projectHandler.CreateProject)
		projectGroup.GET("/:user_id", userHandler.GetUserProjects)
	}
	router.Run(":8080")

}
