package repository

import (
	"main/entity"

	_ "github.com/go-sql-driver/mysql"
	"gorm.io/gorm"
)

type ProjectRepositoryInterface interface {
	CreateProject(project *entity.Project) error
	GetProjectByUserID(id int) ([]entity.Project, error)
}

type ProjectRepository struct {
	db *gorm.DB
}

func NewProjectRepository(db *gorm.DB) ProjectRepositoryInterface {
	return &ProjectRepository{
		db: db,
	}
}

func (repo *ProjectRepository) CreateProject(project *entity.Project) error {
	return repo.db.Create(&project).Error
}

func (repo *ProjectRepository) GetProjectByUserID(id int) ([]entity.Project, error) {
	var projects []entity.Project
	return projects, repo.db.Where("user_id = ?", id).Find(&projects).Error
}
