package repository

import (
	"main/entity"

	_ "github.com/go-sql-driver/mysql"
	"gorm.io/gorm"
)

type UserRepositoryInterface interface {
	CreateUser(user *entity.User) error
	TakeUserByID(id int) (*entity.User, error)
	GetUserPreloadProjects(id int) (*entity.User, error)
}

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepositoryInterface {
	return &UserRepository{
		db: db,
	}
}

func (repo *UserRepository) CreateUser(user *entity.User) error {
	return repo.db.Create(&user).Error
}

func (repo *UserRepository) TakeUserByID(id int) (*entity.User, error) {
	var user *entity.User
	return user, repo.db.Where("id = ?", id).Take(&user).Error
}

func (repo *UserRepository) GetUserPreloadProjects(id int) (*entity.User, error) {
	var user *entity.User
	return user, repo.db.Where("id = ?", id).Preload("Projects").Take(&user).Error
}
