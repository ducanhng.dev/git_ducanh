package dtos

import (
	"time"

	_ "github.com/go-playground/validator/v10"
)

type ProjectRequest struct {
	Name             string `json:"name" binding:"required"`
	ProjectStartedAt string `json:"project_started_at" binding:"required" time_format:"2006-01-02"`
	ProjectEndedAt   string `json:"project_ended_at" time_format:"2006-01-02"`
	UserID           int64  `json:"user_id"`
}

type ProjectResponse struct {
	ID               uint      `json:"id"`
	Name             string    `json:"name"`
	ProjectStartedAt time.Time `json:"project_started_at"`
	ProjectEndedAt   time.Time `json:"project_ended_at"`
	UserID           int64     `json:"user_id"`
}

type ProjectsOfUserResponse struct {
	ID               uint      `json:"id"`
	Name             string    `json:"name"`
	ProjectStartedAt time.Time `json:"project_started_at"`
	ProjectEndedAt   time.Time `json:"project_ended_at"`
}

type ProjectCreateResponse struct {
	Name             string    `json:"name"`
	ProjectStartedAt time.Time `json:"project_started_at"`
	ProjectEndedAt   time.Time `json:"project_ended_at"`
	UserID           int64     `json:"user_id"`
}
