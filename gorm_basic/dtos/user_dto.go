package dtos

import (
	"time"
)

type UserRequest struct {
	FirstName string `json:"first_name" binding:"required"`
	LastName  string `json:"last_name" binding:"required"`
	Email     string `json:"email" binding:"required,email"`
	Password  string `json:"password" binding:"required,min=8,max=12"`
}

type UserResponse struct {
	ID        uint   `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

type UserProjectsResponse struct {
	ID        uint                     `json:"id"`
	FirstName string                   `json:"first_name"`
	LastName  string                   `json:"last_name"`
	Email     string                   `json:"email"`
	Password  string                   `json:"password"`
	CreatedAt time.Time                `json:"created_at"`
	UpdatedAt time.Time                `json:"updated_at"`
	Projects  []ProjectsOfUserResponse `json:"projects" gorm:"foreignkey:ID"`
}
