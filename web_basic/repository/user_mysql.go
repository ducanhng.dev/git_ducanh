package repository

import (
	"database/sql"
	"main/entity"
	"time"
)

type UserMySQL struct {
	db *sql.DB
}

func NewUserMySQL(db *sql.DB) *UserMySQL {
	return &UserMySQL{
		db: db,
	}
}

func (r *UserMySQL) Create(e *entity.User) (entity.ID, error) {
	query, err := r.db.Prepare(`
		insert into users (id, first_name, last_name, email, password, created_at) 
		values
		(?,?,?,?,?,?)`)

	if err != nil {
		return e.ID, err
	}

	_, err = query.Exec(
		e.ID,
		e.First_name,
		e.Last_name,
		e.Email,
		e.Password,
		time.Now(),
	)

	if err != nil {
		return e.ID, err
	}

	err = query.Close()

	if err != nil {
		return e.ID, err
	}

	return e.ID, nil
}

func (r *UserMySQL) Get(id entity.ID) (*entity.User, error) {
	return getUser(id, r.db)
}

func getUser(id entity.ID, db *sql.DB) (*entity.User, error) {
	query, err := db.Prepare(`select id, email, first_name, last_name, created_at from users where id = ?`)
	if err != nil {
		return nil, err
	}
	var u entity.User
	rows, err := query.Query(id)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		err = rows.Scan(&u.ID, &u.Email, &u.First_name, &u.Last_name, &u.Created_at)
	}
	if err != nil {
		return nil, err
	}
	return &u, nil
}

func (r *UserMySQL) List() ([]*entity.User, error) {
	// query, err := r.db.Prepare(`select * from users`)

	// if err != nil {
	// 	return nil, err
	// }

	// var u entity.User

	// rows, err := query.Query()

	// for rows.Next() {

	// }
	return nil, nil
}
