package user

import (
	"database/sql"
	"main/entity"
	"main/repository"
)

type Service struct {
	repo repository.UserMySQL
}

func NewService(db *sql.DB) *Service {
	r := repository.NewUserMySQL(db)

	return &Service{
		repo: *r,
	}
}

func (s *Service) CreateUser(email, password, firstName, lastName string) (entity.ID, error) {
	e, err := entity.NewUser(email, firstName, lastName, password)
	if err != nil {
		return e.ID, err
	}
	return s.repo.Create(e)
}

func (s *Service) GetUser(id entity.ID) (*entity.User, error) {
	return s.repo.Get(id)
}
