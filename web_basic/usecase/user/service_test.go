package user_test

import (
	"database/sql"
	"fmt"
	"log"
	"main/entity"
	"main/usecase/user"
	"os"
	"path/filepath"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
)

func getEnvValue(key string) string {
	err := godotenv.Load(filepath.Join(".env"))

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	return os.Getenv(key)
}

func TestCreate(t *testing.T) {
	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?parseTime=true", getEnvValue("DB_USER"), getEnvValue("DB_PASSWORD"), getEnvValue("DB_HOST"), getEnvValue("DB_DATABASE"))
	db, err := sql.Open("mysql", dataSourceName)
	if err != nil {
		panic(fmt.Errorf("connection fail! %v", err))
	}
	defer db.Close()
	service := user.NewService(db)
	testUser := &entity.User{
		ID:         entity.NewID(),
		First_name: "Test Create",
		Last_name:  "Test Create",
		Email:      "testcreate@gmail.com",
		Password:   "testcreate",
		Created_at: time.Now(),
	}
	_, err = service.CreateUser(testUser.Email, testUser.Password, testUser.First_name, testUser.Last_name)
	assert.Nil(t, err)
	assert.False(t, testUser.Created_at.IsZero())
	assert.True(t, testUser.Updated_at.IsZero())
}
