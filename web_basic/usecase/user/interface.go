package user

import "main/entity"

type UseCase interface {
	CreateUser(email, password, firstName, lastName string) (entity.ID, error)
}
