package loan

import "main/usecase/user"

type Service struct {
	userService user.UseCase
}

func NewService(u user.UseCase) *Service {
	return &Service{
		userService: u,
	}
}
