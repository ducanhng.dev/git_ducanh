package entity

import (
	"time"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID         int
	First_name string
	Last_name  string
	Email      string
	Password   string
	Created_at time.Time
	Updated_at time.Time
}

func HashPassword(password string) (string, error) {
	hashPassword, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		return "", err
	}
	return string(hashPassword), nil
}

func NewUser(email, first_name, last_name, password string) (*User, error) {
	user := &User{
		First_name: first_name,
		Last_name:  last_name,
		Email:      email,
		Created_at: time.Time{},
	}

	pwd, err := HashPassword(password)

	if err != nil {
		return nil, err
	}
	user.Password = pwd
	return user, nil
}
