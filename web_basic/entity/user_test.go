package entity_test

import (
	"main/entity"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewUser(t *testing.T) {
	user, err := entity.NewUser("test@gmail.com", "test", "test", "pass_test")
	assert.Nil(t, err)
	assert.Equal(t, user.First_name, "test")
	assert.NotNil(t, user.ID)
	assert.NotEqual(t, user.Password, "pass_test")
}
