package main

import (
	"database/sql"
	"fmt"
	"log"
	"main/usecase/user"
	"os"
	"path/filepath"

	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

func getEnvValue(key string) string {
	err := godotenv.Load(filepath.Join(".env"))

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	return os.Getenv(key)
}

func ConnectDB() *sql.DB {
	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?parseTime=true", getEnvValue("DB_USER"), getEnvValue("DB_PASSWORD"), getEnvValue("DB_HOST"), getEnvValue("DB_DATABASE"))
	db, err := sql.Open("mysql", dataSourceName)
	if err != nil {
		panic(fmt.Errorf("connection fail! %v", err))
	}
	fmt.Println("Connect complete!")

	return db
}

func main() {
	db := ConnectDB()
	defer db.Close()
	service := user.NewService(db)
	eid, err := service.CreateUser("test@gmail.com", "123456", "Test", "Nguyen")
	if err != nil {
		log.Panic(err)
	}
	fmt.Println("Create employee completely id: ", eid)
	emp, err := service.GetUser(eid)
	if err != nil {
		panic(fmt.Errorf("query fail! %v", err))
	}
	fmt.Println("Employee information: ", emp.Email)
}
