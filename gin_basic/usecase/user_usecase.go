package usecase

import (
	"main/entity"
	"main/repository"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type UserUsecaseInterface interface {
	CreateUser(email, password, firstName, lastName string) error
	TakeUser(id int) (*entity.User, error)
}

type UserUsecase struct {
	// user repository instance
	userRepo repository.UserRepositoryInterface
}

func NewUserUsecase(userRepo repository.UserRepositoryInterface) UserUsecaseInterface {
	return &UserUsecase{
		userRepo,
	}
}

func HashPassword(password string) (string, error) {
	hashPassword, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		return "", err
	}
	return string(hashPassword), nil
}

func NewUser(email, firstName, lastName, password string) (*entity.User, error) {
	user := &entity.User{
		FirstName: firstName,
		LastName:  lastName,
		Email:     email,
		Password:  password,
		CreatedAt: time.Time{},
	}
	return user, nil
}

func (uc *UserUsecase) CreateUser(email string, password string, firstName string, lastName string) error {
	pwdHashed, err := HashPassword(password)
	if err != nil {
		return err
	}
	user, err := NewUser(email, firstName, lastName, pwdHashed)
	if err != nil {
		return err
	}
	return uc.userRepo.CreateUser(user)
}

func (uc *UserUsecase) TakeUser(id int) (*entity.User, error) {
	user, err := uc.userRepo.TakeUserByID(id)
	if err != nil {
		return nil, err
	}
	return user, nil
}
