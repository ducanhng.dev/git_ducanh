package repository

import (
	"database/sql"
	"errors"
	"main/entity"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type UserRepositoryInterface interface {
	CreateUser(user *entity.User) error
	TakeUserByID(id int) (*entity.User, error)
}

type UserRepository struct {
	db *sql.DB
}

func NewUserRepository(db *sql.DB) UserRepositoryInterface {
	return &UserRepository{
		db: db,
	}
}

func (repo *UserRepository) CreateUser(user *entity.User) error {
	query, err := repo.db.Prepare(`insert into users (id, first_name, last_name, email, password, created_at, updated_at) values (?,?,?,?,?,?,?)`)
	if err != nil {
		return err
	}
	defer query.Close()

	_, err = query.Exec(
		user.ID,
		user.FirstName,
		user.LastName,
		user.Email,
		user.Password,
		time.Now(),
		time.Now(),
	)

	if err != nil {
		return err
	}

	return nil
}

func (repo *UserRepository) TakeUserByID(id int) (*entity.User, error) {
	query, err := repo.db.Prepare(`select * from users where id = ?`)
	if err != nil {
		return nil, err
	}
	var u entity.User
	rows, err := query.Query(id)
	defer query.Close()
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		err = rows.Scan(&u.ID, &u.FirstName, &u.LastName, &u.Email, &u.Password, &u.CreatedAt, &u.UpdatedAt)
	}
	defer rows.Close()
	if err != nil {
		return nil, err
	}
	if int(u.ID) == 0 && u.Email == "" && u.Password == "" {
		return nil, errors.New("not found")
	}
	return &u, nil
}
