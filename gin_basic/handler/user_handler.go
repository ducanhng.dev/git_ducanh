package handlers

import (
	"database/sql"
	"main/dtos"
	"main/repository"
	"main/usecase"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// UserHandler struct
type UserHandler struct {
	UserUsecase usecase.UserUsecaseInterface
}

// NewUserHandler func
func NewUserHandler(dbConn *sql.DB) *UserHandler {
	userRepo := repository.NewUserRepository(dbConn)
	userUsecase := usecase.NewUserUsecase(userRepo)
	return &UserHandler{
		UserUsecase: userUsecase,
	}
}

func (h *UserHandler) CreateUser(c *gin.Context) {
	userRequest := dtos.UserRequest{}
	if err := c.BindJSON(&userRequest); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
	}
	userResponse := dtos.UserResponse{
		FirstName: userRequest.FirstName,
		LastName:  userRequest.LastName,
		Email:     userRequest.Email,
		Password:  userRequest.Password,
	}
	err := h.UserUsecase.CreateUser(userRequest.Email, userRequest.Password, userRequest.FirstName, userRequest.LastName)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
	}
	c.IndentedJSON(http.StatusCreated, userResponse)
}

func (h *UserHandler) TakeUser(c *gin.Context) {
	id := c.Param("id")
	intID, err := strconv.Atoi(id)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
	}
	user, err := h.UserUsecase.TakeUser(intID)
	if user == nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
	} else {
		c.IndentedJSON(http.StatusOK, user)
	}
}
