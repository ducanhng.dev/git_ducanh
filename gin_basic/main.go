package main

import (
	"log"
	"main/config"
	handlers "main/handler"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		log.Printf("call to endpoints user")
		c.Next()
	}
}

func main() {
	router := gin.Default()
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "Hello DucAnh!",
		})
	})
	db := config.ConnectDB()
	defer db.Close()
	userHandler := handlers.NewUserHandler(db)
	userGroup := router.Group("/user", logger())
	{
		userGroup.POST("/create", userHandler.CreateUser)
		userGroup.GET("/:id", userHandler.TakeUser)
	}
	router.Run(":8080")
}
