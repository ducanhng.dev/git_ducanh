package main

import (
	"fmt"
)

func fibonacci(f int) int {
	if f <= 1 {
		return f
	}
	return fibonacci(f-2) + fibonacci(f-1)
}

func printFibonacci(n int) {
	for i := 0; i < n; i++ {
		fmt.Println(fibonacci(i))
	}
}

func main() {
	fmt.Println("Lesson 3: ")
	printFibonacci(10)
}
