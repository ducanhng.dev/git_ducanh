package main

import (
	"fmt"
)

func calDet(arr [3][3]int) int {
	return ((arr[0][0] * arr[1][1] * arr[2][2]) + (arr[2][0] * arr[0][1] * arr[1][2]) + (arr[1][0] * arr[2][1] * arr[0][2])) - ((arr[0][2] * arr[1][1] * arr[2][0]) + (arr[0][1] * arr[1][0] * arr[2][2]) + (arr[1][2] * arr[2][1] * arr[0][0]))
}

func main() {
	arr := [3][3]int{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}
	fmt.Println("Lesson 1: det(A) =", calDet(arr))
}
