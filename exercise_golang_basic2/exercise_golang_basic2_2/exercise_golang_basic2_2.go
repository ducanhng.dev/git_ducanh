package main

import (
	"fmt"
	"regexp"
	"strings"
)

func removeDuplicateValues(stringSlice []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range stringSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func convertString(s string) string {
	reg, _ := regexp.Compile("[^A-Za-z0-9]+")
	newString := reg.ReplaceAllString(s, "")
	return newString
}

func main() {
	fmt.Println("Lesson 2: ")

	s := "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
	numWords := len(strings.Fields(s))
	fmt.Println("The numbers of words: ", numWords)
	newString := strings.Fields(s)

	removedString := removeDuplicateValues(newString)

	for i := 0; i < len(removedString); i++ {
		tempString := convertString(removedString[i])
		count := strings.Count(s, tempString)
		fmt.Printf("%v - %d\n", tempString, count)
	}
}
