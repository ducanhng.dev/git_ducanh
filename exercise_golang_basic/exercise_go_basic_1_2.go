package main

import (
	"fmt"
	"math"
)

const pi float64 = 3.14

func calculateCircumference(r int) float64 {
	return float64(r*r) * pi
}

func roundCalculateCircumference(area float64) int {
	return int(math.Round(area))
}

func main() {
	//Bai1
	r := 12
	fmt.Println(calculateCircumference(r))

	//Bai2
	area := calculateCircumference(r)
	fmt.Println(roundCalculateCircumference(area))
}
